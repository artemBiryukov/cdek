package com.example.cdek.configure;

import org.springframework.boot.web.embedded.jetty.JettyServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppContext {
    @Bean
    public ConfigurableServletWebServerFactory webServerFactory() {
        return new JettyServletWebServerFactory();
    }
}
