package com.example.cdek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
public class CdekApplication {

    public static void main(String[] args) {
        SpringApplication.run(CdekApplication.class, args);
    }
}
