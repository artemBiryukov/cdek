package com.example.cdek.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * DAO object for courier.
 */
@Component
public class OrderDaoCourierImpl implements OrderDaoCourier {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Adding order and time of creation to database.
     *
     * @param id - order id.
     * @throws CannotGetJdbcConnectionException
     * @throws DuplicateKeyException
     */
    @Override
    public void addOrder(String id) throws CannotGetJdbcConnectionException, DuplicateKeyException {
        jdbcTemplate.update("INSERT INTO orders(id,creationTime) VALUES (?,NOW()) ", id);
    }
}
