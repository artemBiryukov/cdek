package com.example.cdek.db;

import com.example.cdek.order.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * DAO object for operator.
 */
@Component
public class OrderDaoOperatorImpl implements OrderDaoOperator {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Deleting order from database.
     *
     * @param id - order of id to delete.
     * @throws CannotGetJdbcConnectionException
     */
    @Override
    public void deleteOrder(String id) throws CannotGetJdbcConnectionException {
        jdbcTemplate.update("DELETE FROM orders WHERE id=? ", id);
    }

    /**
     * Method for receiveng all orders from database.
     * @return List of orders.
     * @throws CannotGetJdbcConnectionException
     */
    @Override
    public List<Order> getAllOrders() throws CannotGetJdbcConnectionException {
        List<Order> orders = new ArrayList<>();
        String query = "SELECT * FROM orders  ORDER BY `orders`.`creationTime` ASC";
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(query);
        for (Map<String, Object> row : rows) {
            String id = (String) row.get("id");
            Date date = (Date) row.get("creationTime");
            orders.add(new Order(id, date));
        }
        return orders;
    }

    /**
     * Search order by id.
     * @param id - id of order.
     * @return order object.
     * @throws EmptyResultDataAccessException
     * @throws CannotGetJdbcConnectionException
     */
    @Override
    public Order findOrderById(String id) throws EmptyResultDataAccessException, CannotGetJdbcConnectionException {
        return jdbcTemplate.queryForObject("SELECT * FROM orders WHERE id=" + id, BeanPropertyRowMapper.newInstance(Order.class));
    }
}
