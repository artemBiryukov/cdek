package com.example.cdek.db;

import com.example.cdek.order.Order;
import com.mysql.jdbc.exceptions.MySQLDataException;

import java.util.List;

/**
 * DAO Interface for operator.
 */
public interface OrderDaoOperator {
    void deleteOrder(String id) throws MySQLDataException;

    List<Order> getAllOrders() throws MySQLDataException;

    Order findOrderById(String id) throws MySQLDataException;
}
