package com.example.cdek.db;

/**
 * DAO Interface for courier
 */
public interface OrderDaoCourier {
    void addOrder(String id);
}
