package com.example.cdek.order;

import java.util.Date;

/**
 * Order class(Data transfer object).
 */
public class Order {
    private String id;
    private Date creationTime;

    public Order() {
    }

    public Order(String id, Date date) {
        this.creationTime = date;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
}
