package com.example.cdek.services;

import com.example.cdek.db.OrderDaoCourierImpl;
import com.sun.istack.internal.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

/**
 * Service for courier operations with DAO and rendering courier page.
 */
@Service
public class CourierService {
    @Autowired
    private OrderDaoCourierImpl orderDaoCourier;
    private Logger logger = Logger.getLogger(CourierService.class);

    public void addOrder(String id, Model model) {
        try {
            if (id != null && !id.isEmpty()) {
                orderDaoCourier.addOrder(id);
                model.addAttribute("message", "Заказ успешно добавлен!");
            }
        } catch (DuplicateKeyException e) {
            model.addAttribute("message", "Такой заказ уже есть.");
            logger.info("Order already exists.");
        } catch (CannotGetJdbcConnectionException e) {
            model.addAttribute("message", "Нет соединения с БД");
            logger.warning("No database connection");
        }
    }
}
