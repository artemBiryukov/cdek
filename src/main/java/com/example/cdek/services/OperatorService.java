package com.example.cdek.services;

import com.example.cdek.db.OrderDaoOperatorImpl;
import com.example.cdek.order.Order;
import com.sun.istack.internal.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.Collections;
import java.util.List;

/**
 * Service for operator operations and rendering operator page.
 */
@Service
public class OperatorService {
    @Autowired
    private OrderDaoOperatorImpl orderDaoOperator;
    private Logger logger = Logger.getLogger(OperatorService.class);

    public void getAllOrders(Model model) {
        try {
            List<Order> orders = orderDaoOperator.getAllOrders();
            model.addAttribute("orders", orders);
        } catch (CannotGetJdbcConnectionException e) {
            model.addAttribute("message", "Нет соединения с БД");
            logger.warning("No database connection");
        }
    }

    public void deleteById(String id, Model model) {
        try {
            if (id != null && !id.isEmpty()) {
                orderDaoOperator.deleteOrder(id);
                logger.info("Order " + id + " deleted.");
            }
            List<Order> orders = orderDaoOperator.getAllOrders();
            model.addAttribute("orders", orders);
        } catch (CannotGetJdbcConnectionException e) {
            model.addAttribute("message", "Нет соединения с БД");
            logger.warning("No database connection");
        }
    }

    public void findById(String id, Model model) {
        try {
            if (id != null && !id.isEmpty()) {
                List<Order> order = Collections.singletonList(orderDaoOperator.findOrderById(id));
                model.addAttribute("orders", order);
            } else {
                List<Order> orders = orderDaoOperator.getAllOrders();
                model.addAttribute("orders", orders);
            }
        } catch (EmptyResultDataAccessException e) {
            model.addAttribute("message", "Такого заказа нет.");
            logger.warning("Order doesn't exist");
        } catch (CannotGetJdbcConnectionException e) {
            model.addAttribute("message", "Нет соединения с БД");
            logger.warning("No database connection");
        }
    }
}
