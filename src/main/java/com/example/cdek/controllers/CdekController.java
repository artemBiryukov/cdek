package com.example.cdek.controllers;

import com.example.cdek.services.CourierService;
import com.example.cdek.services.OperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * This is the main controller of the application.
 */
@Controller
public class CdekController {
    private final CourierService courierService;
    private final OperatorService operatorService;

    @Autowired
    public CdekController(CourierService courierService, OperatorService operatorService) {
        this.courierService = courierService;
        this.operatorService = operatorService;
    }

    /**
     * Root mapping for index page of the application.
     *
     * @return index page of the app.
     */

    @RequestMapping(value = "/", method = RequestMethod.GET)
    private String index() {
        return "index";
    }

    /**
     * This method is responsible for rendering a courier page
     * and adding orders to database.
     *
     * @param id    - id of the order.
     * @param model - model interface object for rendering.
     * @return - view of courier page;
     */

    @RequestMapping(value = "/courier", method = RequestMethod.GET)
    private String courierPage(@RequestParam(required = false) String id, Model model) {
        courierService.addOrder(id, model);
        return "courier";
    }

    /**
     * This method is responsible for rendering an operator page
     * and show all orders from the database.
     *
     * @param model - model interface object.
     * @return - view of operator page with all orders.
     */

    @RequestMapping(value = "/operator", method = RequestMethod.GET)
    private String operatorPage(Model model) {
        operatorService.getAllOrders(model);
        return "operator";
    }

    /**
     * This method is responsible for deleting orders by id from request.
     *
     * @param id    - order id to delete.
     * @param model - model after rendering.
     * @return - rendered operator page with orders.
     */

    @RequestMapping(value = "/deleteById", method = RequestMethod.GET)
    private String deleteById(@RequestParam String id, Model model) {
        operatorService.deleteById(id, model);
        return "operator";
    }

    /**
     * This method is responsible for finding orders by id from request.
     *
     * @param id    - order id to find.
     * @param model - model after rendering.
     * @return - rendered operator page with orders.
     */

    @RequestMapping(value = "/findById")
    private String findById(@RequestParam String id, Model model) {
        operatorService.findById(id, model);
        return "operator";
    }

    @RequestMapping(value = "/403")
    private String accesError() {
        return "403";
    }
}
