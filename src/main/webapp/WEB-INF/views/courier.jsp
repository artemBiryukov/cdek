<%@ page contentType="text/html;charset=utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru">
<head>
    <title>CDEK courier service</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="../../css/operator.css" type="text/css">
</head>
<body>
<h1>Номер заказа:</h1>
<div>
    <form action="/courier" method="GET">
        <input type="text" name="id">
        <input type="submit" value="Не успеваю">
    </form>
    <form action="/">
        <input type="submit" value="На главную"/>
    </form>
    <form action="/logout">
        <input type="submit" value="Выйти из аккаунта"/>
    </form>
    <p>${message}</p>
</div>
</body>
</html>