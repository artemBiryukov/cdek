<%@ page contentType="text/html;charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru">
<head>
    <title>CDEK operator service</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="../../css/operator.css" type="text/css">
</head>
<body>
<div>
    <h3>Поиск по заказу</h3>
    <form action="/findById">
        <input type="text" name="id"/>
        <input type="submit" value="Найти"/>
        <form action="/operator">
            <input type="submit" value="Назад"/>
        </form>
    </form>
    <form action="/">
        <input type="submit" value="На главную"/>
    </form>
    <form action="/logout">
        <input type="submit" value="Выйти из аккаунта"/>
    </form>
</div>
<div>
    <table border="1">
        <th>Номер заказа</th>
        <th>Время создания</th>
        <c:forEach items="${orders}" var="orders">
            <tr>
                <td>
                    <c:out value="${orders.getId()}"/>
                </td>
                <td>
                    <c:out value="${orders.getCreationTime()}"/>
                </td>
                <td>
                    <form action="/deleteById" method="get">
                        <input type="hidden" name="id" value="${orders.getId()}"/>
                        <input type="submit" value="Удалить"/>
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
    <p>${message}</p>
</div>
</body>
</html>