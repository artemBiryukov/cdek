<%@page contentType="text/html;charset=utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>CDEK homepage</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../css/operator.css" type="text/css">
</head>
<body>
<header>
    <h1>CDEK Company</h1>
</header>
<main>
    <form action="/operator">
        <input type="submit" value="Оператор"/>
    </form>
    <form action="/courier">
        <input type="submit" value="Курьер"/>
    </form>
</main>
</body>
</html>